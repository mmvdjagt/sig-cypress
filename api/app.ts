import express = require("express");
import cors from "cors";
import http from "http";
import moment from "moment";
import "moment/locale/nl";

const startServer = () => {
    const app = express();
    app.use(cors());

    app.get("/", (_, res) => {
        res.send("SIG Cypress API");
    });

    app.get("/greeting", (_, res) => {
        moment.locale("nl");
        const day = moment().format("dddd");
        return res.status(200).json(`Goedendag, vandaag is het ${day}`);
    });

    app.get("/date", (_, res) => {
        console.log("date endpoint aangeroepen");
        const seed = Math.round(Math.random());
        if (seed === 0)
            return res.status(500).json({ error: "Haha ik sloop je test" });

        return res.status(200).json({ date: moment().format("YYYY-MM-DD") });
    });

    app.get("/users", async (_, res) => {
        await new Promise(r => setTimeout(r, 7000));
        return res.status(200).json({
            users: [
                { id: 1, name: "Jan", country: "nl" },
                { id: 2, name: "Piet", country: "nl" },
                { id: 3, name: "Henk", country: "nl" },
                { id: 4, name: "Henry", country: "gb" }
            ]
        });
    });

    const httpServer = http.createServer(app);
    httpServer.listen(3333, () => console.log("API running on port 3333"));
};

startServer();