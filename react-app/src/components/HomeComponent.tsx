import React from 'react';
import { withStyles, WithStyles, createStyles, Theme, Typography } from '@material-ui/core';

interface IProps { }

type ClassKey = 'root'
type PropsType = IProps & WithStyles<ClassKey>
const styles = (theme: Theme) => createStyles<ClassKey, Record<string, any>>({
    root: {}
});

const HomeComponent: React.FC<PropsType> = (props) => {
    return (
        <Typography variant="h3">Cypress SIG</Typography>
    );
};

export default withStyles(styles)(HomeComponent);