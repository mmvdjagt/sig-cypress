import React from 'react';
import { withStyles, WithStyles, createStyles, Theme, Table, TableHead, TableRow, TableCell, TableBody } from '@material-ui/core';
import { SigUser } from "./Opdracht3";

interface IProps {
    users: SigUser[]
};

type ClassKey = 'root'
type PropsType = IProps & WithStyles<ClassKey>
const styles = (theme: Theme) => createStyles<ClassKey, Record<string, any>>({
    root: {}
});

const UsersTable: React.FC<PropsType> = (props) => {
    const { users } = props;

    return (
        <Table id="usersTable">
            <TableHead>
                <TableRow>
                    <TableCell>ID</TableCell>
                    <TableCell>Naam</TableCell>
                    <TableCell>Land</TableCell>
                </TableRow>
            </TableHead>
            <TableBody id="usersTableBody">
                {users.map(u => (
                    <TableRow cy-class="userRow" key={u.id}>
                        <TableCell cy-class="userId">{u.id}</TableCell>
                        <TableCell cy-class="userName">{u.name}</TableCell>
                        <TableCell cy-class="userCountry">{u.country}</TableCell>
                    </TableRow>
                ))}
            </TableBody>
        </Table>
    );
};

export default withStyles(styles)(UsersTable);