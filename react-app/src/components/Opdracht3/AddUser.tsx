import React, { ChangeEvent, useState } from 'react';
import { withStyles, WithStyles, createStyles, Theme, Dialog, DialogTitle, DialogContent, Grid, TextField, DialogActions, Button } from '@material-ui/core';
import { SigUser } from './Opdracht3';

interface IProps {
    open: boolean,
    onClose: (newUser?: SigUser) => void
};

type ClassKey = 'root'
type PropsType = IProps & WithStyles<ClassKey>
const styles = (theme: Theme) => createStyles<ClassKey, Record<string, any>>({
    root: {}
});

const AddUser: React.FC<PropsType> = (props) => {
    const { open, onClose } = props;
    const [id, setId] = useState<number>();
    const [name, setName] = useState<string>();
    const [country, setCountry] = useState<string>();

    const cancel = () => {
        onClose();
    };

    const confirm = () => {
        if (id && name && country)
            onClose({ id, name, country });
    };

    const idChanged = (event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        if (isNaN(+event.target.value)) return;
        setId(+event.target.value);
    };

    const nameChanged = (event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        setName(event.target.value);
    };

    const countryChanged = (event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        setCountry(event.target.value);
    };

    return (
        <Dialog open={open} onClose={cancel} disableBackdropClick disableEscapeKeyDown fullWidth maxWidth="md">
            <DialogTitle>Gebruiker toevoegen</DialogTitle>
            <DialogContent>
                <Grid container spacing={3}>
                    <Grid item xs={4}>
                        <TextField id="inputId" fullWidth type="number" label="ID" value={id} onChange={idChanged} />
                    </Grid>
                    <Grid item xs={4}>
                        <TextField id="inputName" fullWidth value={name} label="Naam" onChange={nameChanged} />
                    </Grid>
                    <Grid item xs={4}>
                        <TextField id="inputCountry" fullWidth value={country} label="Land" onChange={countryChanged} />
                    </Grid>
                </Grid>
            </DialogContent>
            <DialogActions>
                <Button variant="text" onClick={cancel}>Annuleren</Button>
                <Button id="saveButtonAddUser" variant="contained" color="primary" onClick={confirm} disabled={!id || isNaN(id) || !name || !country}>Opslaan</Button>
            </DialogActions>
        </Dialog>
    );
};

export default withStyles(styles)(AddUser);