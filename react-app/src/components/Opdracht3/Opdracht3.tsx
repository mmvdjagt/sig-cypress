import React, { ChangeEvent, useEffect, useState } from 'react';
import { withStyles, WithStyles, createStyles, Theme, Typography, Select, MenuItem, Button, FormControl, InputLabel } from '@material-ui/core';
import UsersTable from './UsersTable';
import AddUser from './AddUser';

interface IProps { }

type ClassKey = 'root'
type PropsType = IProps & WithStyles<ClassKey>
const styles = (theme: Theme) => createStyles<ClassKey, Record<string, any>>({
    root: {}
});

export interface SigUser {
    id: number,
    name: string,
    country: string
};

export type SortOrder = "asc" | "desc";

const Opdracht3: React.FC<PropsType> = (props) => {
    const [users, setUsers] = useState<SigUser[]>();
    const [sortOrder, setSortOrder] = useState<SortOrder>("asc");
    const [addUserOpen, setAddUserOpen] = useState<boolean>(false);

    useEffect(() => {
        const fetchData = async () => {
            const response = await (await fetch("http://localhost:3333/users")).json();
            setUsers(response.users);
        };

        fetchData();
    }, []);

    const sortUsers = () => {
        if (users?.length) {
            const usersCopy = [...users];
            setUsers(usersCopy.sort((a, b) => {
                if (sortOrder === "asc")
                    return a.name < b.name ? -1 : 1;
                return a.name < b.name ? 1 : -1;
            }));
        }
    };

    const onSelectChange = (event: ChangeEvent<{ value: unknown }>) => {
        setSortOrder(event.target.value as SortOrder);
    };

    const onAddUserDialogClose = (newUser?: SigUser) => {
        setAddUserOpen(false);
        if (newUser && users)
            setUsers([...users, newUser]);
    };

    if (!users) return <Typography variant="body2">Loading...</Typography>

    return (
        <>
            <div style={{ width: "100%", display: "flex", alignItems: "center" }}>
                <FormControl style={{ width: 200, textAlign: "left", marginLeft: 8 }}>
                    <InputLabel>Sorteervolgorde</InputLabel>
                    <Select id="sortSelect" value={sortOrder} onChange={onSelectChange}>
                        <MenuItem id="itemAsc" value="asc">Oplopend</MenuItem>
                        <MenuItem id="itemDesc" value="desc">Aflopend</MenuItem>
                    </Select>
                </FormControl>
                <Button id="sortButton" variant="contained" color="primary" onClick={sortUsers} style={{ marginLeft: 8, height: 40 }}>Sorteer</Button>
            </div>
            <Button id="addUserButton" variant="contained" color="secondary" onClick={() => setAddUserOpen(true)}>Gebruiker toevoegen</Button>
            <UsersTable users={users} />
            <AddUser open={addUserOpen} onClose={onAddUserDialogClose} />
        </>
    );
};

export default withStyles(styles)(Opdracht3);