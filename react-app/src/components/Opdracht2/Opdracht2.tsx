import React, { useEffect, useState } from 'react';
import { withStyles, WithStyles, createStyles, Theme, Typography, Button } from '@material-ui/core';
import moment from "moment";
import "moment/locale/nl";

interface IProps { }

type ClassKey = 'root'
type PropsType = IProps & WithStyles<ClassKey>
const styles = (theme: Theme) => createStyles<ClassKey, Record<string, any>>({
    root: {}
});

const Opdracht2: React.FC<PropsType> = (props) => {
    const [greeting, setGreeting] = useState<string>();
    const [date, setDate] = useState<string>();

    useEffect(() => {
        const fetchData = async () => {
            const response = await (await fetch("http://localhost:3333/greeting")).json();
            setGreeting(response);
        };

        fetchData();
        moment.locale("nl");
    }, []);

    const getDate = async () => {
        const response = await fetch("http://localhost:3333/date");
        if (response.status === 200) {
            const parsedRes = await response.json();
            setDate(`De datum is ${moment(parsedRes.date).format("DD MMMM YYYY")}`);
        }
        else
            setDate("Er is iets fout gegaan :<");
    };

    return (
        <>
            <Typography id="greeting" variant="h5">{greeting}</Typography>
            {date && <Typography id="date" variant="body1">{date}</Typography>}
            <Button id="getDateButton" onClick={getDate} variant="contained" color="primary">Wat is de datum</Button>
        </>
    );
};

export default withStyles(styles)(Opdracht2);