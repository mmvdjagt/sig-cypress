import React, { useState } from 'react';
import { withStyles, WithStyles, createStyles, Theme, Drawer, List, ListItem, ListItemText, IconButton } from '@material-ui/core';
import MenuIcon from "@material-ui/icons/Menu";
import { useHistory } from 'react-router';

interface IProps { }

type ClassKey = 'root'
type PropsType = IProps & WithStyles<ClassKey>
const styles = (theme: Theme) => createStyles<ClassKey, Record<string, any>>({
    root: {}
});

const Sidebar: React.FC<PropsType> = (props) => {
    const [sidebarOpen, setSidebarOpen] = useState<boolean>(false);
    const history = useHistory();

    const toggleSidebar = () => {
        setSidebarOpen(!sidebarOpen);
    };

    const listItems = ["Opdracht 1", "Opdracht 2", "Opdracht 3"];

    const onItemClick = (item: string) => {
        history.push(item.replace(" ", "").toLowerCase());
        setSidebarOpen(false);
    };

    return (
        <>
            <IconButton color="inherit" edge="start" onClick={toggleSidebar}>
                <MenuIcon />
            </IconButton>
            <Drawer open={sidebarOpen} onClose={toggleSidebar}>
                <List>
                    {listItems.map((item, i) => (
                        <ListItem button key={i} onClick={_ => onItemClick(item)}>
                            <ListItemText primary={item} />
                        </ListItem>
                    ))}
                </List>
            </Drawer>
        </>
    );
};

export default withStyles(styles)(Sidebar);