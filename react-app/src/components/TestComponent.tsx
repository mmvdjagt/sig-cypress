import React from 'react';
import { withStyles, WithStyles, createStyles, Theme } from '@material-ui/core';
import Appbar from './Appbar';
import { Route, Switch } from 'react-router';
import Opdracht1 from './Opdracht1/Opdracht1';
import Opdracht2 from './Opdracht2/Opdracht2';
import HomeComponent from './HomeComponent';
import { BrowserRouter } from 'react-router-dom';
import Opdracht3 from './Opdracht3/Opdracht3';

interface IProps { }

type ClassKey = 'root'
type PropsType = IProps & WithStyles<ClassKey>
const styles = (theme: Theme) => createStyles<ClassKey, Record<string, any>>({
    root: {}
});

const TestComponent: React.FC<PropsType> = (props) => {
    return (
        <BrowserRouter>
            <Appbar />
            <Switch>
                <Route path="/opdracht1" component={Opdracht1} />
                <Route path="/opdracht2" component={Opdracht2} />
                <Route path="/opdracht3" component={Opdracht3} />
                <Route component={HomeComponent} />
            </Switch>
        </BrowserRouter>
    );
};

export default withStyles(styles)(TestComponent);