import React, { useState } from 'react';
import { withStyles, WithStyles, createStyles, Theme, TextField, Typography, Button, Divider } from '@material-ui/core';

interface IProps { }

type ClassKey = 'root'
type PropsType = IProps & WithStyles<ClassKey>
const styles = (theme: Theme) => createStyles<ClassKey, Record<string, any>>({
    root: {}
});

const Opdracht1: React.FC<PropsType> = (props) => {
    const [visibleText, setVisibleText] = useState<string>("Dit is een zeer spannende eerste opdracht");
    const [inputText, setInputText] = useState<string>();

    const setNewText = () => {
        setVisibleText(inputText?.toUpperCase() ?? "");
    };

    const clearInputField = () => {
        setInputText("");
    };

    return (
        <>
            <Typography variant="h5" cy-id="cyVisibleText">{visibleText}</Typography>
            <Divider />
            <div style={{ marginTop: 8 }}>
                <TextField inputProps={{ "cy-id": "cyInputField" }} value={inputText} onChange={event => setInputText(event.target.value)} />
                <Button id="capsifyButton" onClick={setNewText} variant="contained" color="primary" style={{ marginLeft: 8 }}>Capsify</Button>
                <Button id="clearButton" onClick={clearInputField} variant="contained" color="secondary" style={{ marginLeft: 8 }}>Clear</Button>
            </div>
        </>
    );
};

export default withStyles(styles)(Opdracht1);