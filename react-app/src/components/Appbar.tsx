import React from 'react';
import { withStyles, WithStyles, createStyles, Theme, AppBar, Toolbar } from '@material-ui/core';
import Sidebar from './Sidebar';

interface IProps { }

type ClassKey = 'root'
type PropsType = IProps & WithStyles<ClassKey>
const styles = (theme: Theme) => createStyles<ClassKey, Record<string, any>>({
    root: {}
});

const Appbar: React.FC<PropsType> = (props) => {
    return (
        <AppBar position="static">
            <Toolbar>
                <Sidebar />
            </Toolbar>
        </AppBar>
    );
};

export default withStyles(styles)(Appbar);