describe("Test suite opdracht 1", () => {
    before(() => {
        cy.visit("http://localhost:3000/opdracht1");
    });

    it("toont de juiste standaard tekst", () => {
        cy.get("[cy-id=cyVisibleText]")
          .contains("Dit is een zeer spannende eerste opdracht");
    });
});