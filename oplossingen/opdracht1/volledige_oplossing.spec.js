// Er zijn natuurlijk veel verschillende manieren om je tests op te stellen; dit is maar een voorbeeld van hoe je het zou kunnen aanpakken

describe("Test suite opdracht 1", () => {
    before(() => {
        cy.visit("http://localhost:3000/opdracht1");
    });

    it("toont de juiste standaard tekst", () => {
        cy.get("[cy-id=cyVisibleText]")
          .contains("Dit is een zeer spannende eerste opdracht");
    });

    it("capsifiet de tekst op de juiste manier", () => {
        cy.get("[cy-id=cyInputField")
          .type("Test input");

        cy.get("#capsifyButton")
          .click();

        cy.get("[cy-id=cyVisibleText]")
          .contains("TEST INPUT");
    });

    it("verandert de titel niet bij het typen in het inputveld", () => {
        cy.get("[cy-id=cyInputField")
          .as("inputField")
          .clear()
          .type("Input test 3a");

        cy.get("#capsifyButton")
          .click();

        cy.get("[cy-id=cyVisibleText]")
          .as("title")
          .contains("INPUT TEST 3A");

        cy.get("@inputField")
          .clear()
          .type("Input test 3b");

        cy.get("@title")
          .contains("INPUT TEST 3A");

        // of een andere optie
        cy.get("@title")
          .should("not.contain", "INPUT TEST 3B");
    });

    it("verwijdert de tekst in het inputveld na het klikken op de clear knop", () => {
        cy.get("[cy-id=cyInputField]")
          .as("inputField")
          .type("extra tekst");

        cy.get("#clearButton")
          .click();

        cy.get("@inputField")
          .invoke("val")
          .should("equal", "");
    });
});