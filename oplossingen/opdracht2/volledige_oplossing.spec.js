import moment from "moment";
import "moment/locale/nl";

describe("Test suite opdracht 2", () => {
  before(() => {
    cy.intercept({ method: "GET", url: "http://localhost:3333/greeting" })
      .as("apiGreeting");

    cy.visit("http://localhost:3000/opdracht2");
    moment.locale("nl");
  });

  it("haalt de juiste greeting op van de server", () => {
    cy.wait("@apiGreeting");

    const weekDay = moment().format("dddd");
    cy.get("#greeting")
      .invoke("text")
      .should("equal", `Goedendag, vandaag is het ${weekDay}`);
  });

  it("haalt de datum op van de server", () => {
    cy.intercept({ method: "GET", url: "http://localhost:3333/date" }, { date: "2021-05-31" })
      .as("apiDate");

    cy.get("#getDateButton")
      .click();

    cy.wait("@apiDate");

    cy.get("#date")
      .invoke("text")
      .should("equal", `De datum is 31 mei 2021`);
  });
});