describe("Test suite opdracht 3", () => {
  before(() => {
    // Het users.json bestand moet in de cypress/fixtures folder staan
    cy.intercept({ method: "GET", url: "http://localhost:3333/users" }, { fixture: "users.json" })
      .as("getUsers");

    cy.visit("http://localhost:3000/opdracht3");
  });

  it("haalt de gebruikers op uit de server", () => {
    cy.wait("@getUsers");

    cy.get("#usersTableBody")
      .children()
      .as("usersRows")
      .should("have.length", 8);

    cy.get("[cy-class=userId]")
      .then(cells => {
        const userIds = cells.toArray().map(c => c.innerHTML);
        const sorted = [...userIds].sort((a, b) => a < b ? -1 : 1);
        expect(userIds).to.eql(sorted);
      });
  });

  it("sorteert de gebruikers op naam", () => {
    cy.get("#sortSelect")
      .as("sortSelect")
      .click();

    cy.get("#itemAsc")
      .click();

    cy.get("#sortButton")
      .as("sortButton")
      .click();

    cy.get("[cy-class=userName]")
      .then(cells => {
        const usernames = cells.toArray().map(c => c.innerHTML);
        const sorted = [...usernames].sort((a, b) => a < b ? -1 : 1);
        expect(usernames).to.eql(sorted);
      });

    cy.get("@sortSelect")
      .click();

    cy.get("#itemDesc")
      .click();

    cy.get("@sortButton")
      .click();

    cy.get("[cy-class=userName]")
      .then(cells => {
        const usernames = cells.toArray().map(c => c.innerHTML);
        const sorted = [...usernames].sort((a, b) => a < b ? 1 : -1);
        expect(usernames).to.eql(sorted);
      });
  });

  it("kan een nieuwe gebruiker toevoegen", () => {
    cy.get("[cy-class=userName]")
      .as("oldUsernames");

    cy.get("#addUserButton")
      .click();

    cy.get("#saveButtonAddUser")
      .as("saveButton")
      .should("be.disabled");

    cy.get("#inputId")
      .type(20);

    cy.get("#inputName")
      .type("Karel");

    cy.get("#inputCountry")
      .type("be");

    cy.get("@saveButton")
      .should("not.be.disabled")
      .click();

    cy.get("[cy-class=userName]")
      .then(cells => {
        const newUsernames = cells.toArray().map(c => c.innerHTML);

        cy.get("@oldUsernames")
          .then(oldCells => {
            const oldUsernames = oldCells.toArray().map(c => c.innerHTML);
            expect([...oldUsernames, "Karel"]).to.eql(newUsernames);
          });
      });
  });
});